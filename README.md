# Doc pour docker

## Interne

* La complétion est bien foutue.
* Le `--help` de `docker` est contextuel:
	* `docker --help`
	* `docker network --help`
	* `docker network create --help`

## Externe

* [Official CLI cheatsheet](docker_cheatsheet.pdf)
* [docs.docker.com](https://docs.docker.com)
	* [Docker CLI reference](https://docs.docker.com/engine/reference/commandline/docker/)
	* [Dockerfile reference](https://docs.docker.com/engine/reference/builder/)
	* [Docker composer](https://docs.docker.com/compose/compose-file/), avec notamment :
		* [Services](https://docs.docker.com/compose/compose-file/05-services/)
		* [Network](https://docs.docker.com/compose/compose-file/06-networks/)
		* [Volumes](https://docs.docker.com/compose/compose-file/07-volumes/)
